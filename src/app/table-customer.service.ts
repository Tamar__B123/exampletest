import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TableCustomerService {

  
  customersCollection:AngularFirestoreCollection = this.db.collection('customers'); 


  
  getCustomers(startAfter):Observable<any[]> {
  this.customersCollection = this.db.collection(`customers`,
     ref => ref.orderBy('predict', 'asc').limit(10).startAfter(startAfter));
  return this.customersCollection.snapshotChanges();    
  }

//דפדוף אחורה
getCustomers2(endBefore):Observable<any[]> {
  this.customersCollection = this.db.collection(`customers`,
     ref => ref.orderBy('predict', 'asc').limit(10).endBefore(endBefore));
  return this.customersCollection.snapshotChanges();    
}

  // getCustomersnopage(){
  //   this.customersCollection = this.db.collection(`customers`, 
  //   ref =>ref.orderBy('predict', 'asc'));
  //   return this.customersCollection.snapshotChanges()
  // }

  getCustomersLeave():Observable<any[]> {
    this.customersCollection = this.db.collection(`customers`,
       ref => ref.where("predict", "==", "will leave").limit(10));
    return this.customersCollection.snapshotChanges();    
  }

  getCustomersleavenext(startAfter):Observable<any[]> {
    this.customersCollection = this.db.collection(`customers`,
       ref => ref.where("predict", "==", "will leave").limit(10).startAfter(startAfter));
    return this.customersCollection.snapshotChanges();    
  }
  
  //לקוחות עוזבים- דפדוף אחורה
getCustomersLeave2(endBefore):Observable<any[]> {
  this.customersCollection = this.db.collection(`customers`,
     ref => ref.where("predict", "==", "will leave").limit(10).endBefore(endBefore));
  return this.customersCollection.snapshotChanges();    
}

  deleteCustomer(id:string){
    this.db.doc(`customers/${id}`).delete();
  }

  updateCustomer(CustomerId:string, predict:string){
    this.db.doc(`customers/${CustomerId}`).update(
      {
        predict:predict
      }
    )
  }

  addcustomer(age:string,more_than_1_card:string,student:string, main_city_sail:string, Utilizing_card_1:number,Utilizing_card_2:number,
    Utilizing_card_3:number,Utilizing_card_4:number,perc_private:number, perc_proff:number,name:string,email:string,phone:string,predict){
    const customer = {age:age, more_than_1_card:more_than_1_card, student:student, main_city_sail:main_city_sail, Utilizing_card_1:Utilizing_card_1,
      Utilizing_card_2:Utilizing_card_2, Utilizing_card_3:Utilizing_card_3,Utilizing_card_4:Utilizing_card_4,
      perc_private:perc_private,perc_proff:perc_proff,name:name,email:email,phone:phone,predict:predict}; 
    this.customersCollection.add(customer);
  }

  
// editCustomer(id:string,more_than_1_card:string,student:string, main_city_sail:string, Utilizing_card_1:number,Utilizing_card_2:number,
//   Utilizing_card_3:number,Utilizing_card_4:number,perc_private:number, perc_proff:number,name:string,email:string,phone:string){
//   this.db.doc(`customers/${id}`).update(
//     {
//       age:age, more_than_1_card:more_than_1_card, student:student, main_city_sail:main_city_sail, Utilizing_card_1:Utilizing_card_1,
//         Utilizing_card_2:Utilizing_card_2, Utilizing_card_3:Utilizing_card_3,Utilizing_card_4:Utilizing_card_4,
//         perc_private:perc_private,perc_proff:perc_proff,name:name,email:email,phone:phone
//     }
//   )
// }

editCustomer(id:string,more_than_1_card:string,name:string,email:string,phone:string){
  this.db.doc(`customers/${id}`).update(
    {
      more_than_1_card:more_than_1_card, name:name,email:email,phone:phone
    }
  )
}

  add(age:string){
    const customer = {age:age}; 
    this.customersCollection.add(customer);
  }

  
  constructor(private db:AngularFirestore) { }

}
