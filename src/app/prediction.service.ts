import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {

  private url = 'https://jh2baj3cg8.execute-api.us-east-1.amazonaws.com/beta';

  predict(years:number, income:number):Observable<any>{ //הפונקציה מקבלת שנות לימוד והכנסה ומבצעת את החיזוי
    let json = { //בניית הגייסון במבנה שהאיי פיי איי גייטואי מקבל
      "data": 
        {
          "years": years,
          "income": income
        }
    }
    let body  = JSON.stringify(json); //הפיכה של הגייסון לסטרינג
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe( //מחזירים אובסרבבול
      map(res => { //נטען את מאפ 
        console.log("in the map"); //בדיקה שהגענו לפונקציה מאפ
        console.log(res); //נדפיס את התוצאה שמתקבלת מהשרת
        console.log(res.body); //מדפיבים את החלק של החיזוי באחוזים
        return res.body; //מחזירים את ההסתברות להחזרה     
      })
    );      
  }

  constructor(private http:HttpClient) { }
}
