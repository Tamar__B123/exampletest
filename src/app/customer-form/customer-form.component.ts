import { TableCustomerService } from './../table-customer.service';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'appcustomerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})

export class CustomerFormComponent implements OnInit {
  age;
  more_than_1_card;
  student;
  main_city_sail;
  Utilizing_card_1;
  Utilizing_card_2;
  Utilizing_card_3;
  Utilizing_card_4;
  perc_private;
  perc_proff;
  name;
  email;
  phone;
  predict="";
  selected;


  constructor(private router:Router,
    public authService:AuthService,
    private tableCustomerService:TableCustomerService) { }

  save(){
      this.tableCustomerService.addcustomer(this.age,this.more_than_1_card, this.student, this.main_city_sail,this.Utilizing_card_1,
        this.Utilizing_card_2,this.Utilizing_card_3,this.Utilizing_card_4,
        this.perc_private,this.perc_proff,this.name, this.email,this.phone,this.predict); 
      this.router.navigate(['/customers']); 
  }

//   save(){
//     console.log(this.age);
//     this.tableCustomerService.add(this.age); 
// }

  ngOnInit(): void {
  }

}
