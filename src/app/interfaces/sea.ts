export interface Sea {
    lat: number,
    lon: number,
    model :string,
    parameters,
    levels,
    key: string,
}
