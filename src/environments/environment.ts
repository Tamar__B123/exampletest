// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyCRicW77hsWaMeHQFimAn87DOEJmmBkw6M",
    authDomain: "tamar-exampletest.firebaseapp.com",
    projectId: "tamar-exampletest",
    storageBucket: "tamar-exampletest.appspot.com",
    messagingSenderId: "786078665926",
    appId: "1:786078665926:web:5f4759b6ad79e9187f5bc7"
  }
};
//


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
